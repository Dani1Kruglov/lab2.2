#include <iostream>
#include <chrono>

int n;

void QUICKSORT(int mas[n], int a, int b)
{
    if(a >=b)
        return;
    int m, k, l, r;
    m =  rand() % (b - a + 1) + a;
    k = mas[m];
    l = a - 1;
    r = b + 1;
    while(1)
    {
        do l = l + 1; while (mas[l] < k);
        do r = r - 1; while (mas[r] > k);
        if( l >= r)
            break;
        std::swap(mas[l],mas[r]);
    }
    r = l;
    l = l - 1;
    QUICKSORT( mas, a, l);
    QUICKSORT( mas, r, b);

}

class Timer
{
private:

    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<double, std::ratio<1> >;

    std::chrono::time_point<clock_t> m_beg;

public:
    Timer() : m_beg(clock_t::now())
    {
    }

    void reset()
    {
        m_beg = clock_t::now();
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};


int main()
{
    int n;
    std::cout<<"Количество элементов в последовательности: ";
    std::cin>> n;
    int mas[n];
    for(int i = 0; i < n; i ++)
        mas[i] = rand();
    Timer time;
    int a = 0;
    int b = n - 1;
    QUICKSORT(mas, a, b);
    std::cout << "Time elapsed: " << time.elapsed() << '\n';
    return 0;
}


//при увеличении колчества элементов также увеличивается и время работы алгоритма. Связано, по моему мнению, это с тем, что компьютер обрабатывает намного больше элементов.